package com.Theucalyptus.plugin_lg;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class LG_EventHandlers implements Listener {
	
	private final LG plugin;
	
	LG_EventHandlers(LG plugin) {
		this.plugin = plugin;
	}
	
	
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		event.getPlayer().setGameMode(GameMode.SPECTATOR);
		if (plugin.getConfig().getBoolean("General.pseudo_visible") == false) {
			event.getPlayer().setCustomName("");
			event.getPlayer().setCustomNameVisible(false);
		}	
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		if(plugin.game_manager.isInGame()) {
			plugin.player_manager.removeLGPlayer(event.getPlayer().getUniqueId());
		}	
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		if (plugin.game_manager.isPaused()) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerDamage(EntityDamageEvent event) {
		if (plugin.game_manager.isPaused()) {
			event.setCancelled(true);
		}
	}


}
