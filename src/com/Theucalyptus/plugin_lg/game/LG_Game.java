package com.Theucalyptus.plugin_lg.game;

import java.util.LinkedList;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import com.Theucalyptus.plugin_lg.LG;
import com.Theucalyptus.plugin_lg.LG_Utils;
import com.Theucalyptus.plugin_lg.data.LG_Roles;
import com.Theucalyptus.plugin_lg.players.LG_Player;


	
	
public class LG_Game {
		
		private LG plugin;	
		private int inGameTime = 1; //temps en seconde
		private int dayCount = 0;
		private boolean vote_ouvert = false;
		
		
		public LG_Game(LG plugin) {
			this.plugin = plugin;
		}
		
		private BukkitRunnable mainRunnable = new BukkitRunnable() {
				
			@Override 
			public void run() {
				LG plugin = (LG) JavaPlugin.getPlugin(LG.class);
				plugin.game_manager.getGame().update();
			}
		};
		public void startMainRunnable() {
			mainRunnable.runTaskTimer(plugin, 20, 20); //update toutes les secondes
		}
		public void stopMainRunnable() {
			Bukkit.getScheduler().cancelTask(mainRunnable.getTaskId());
		}
	
		public boolean isVoteOuvert() {
			return vote_ouvert;
		}
		
		public void update() {
			
			if(plugin.game_manager.isPaused() == true) 
				return;
			else {
				inGameTime++;
				int old_dayCount = dayCount;
				int dayLength = plugin.getConfig().getInt("General.duree_episode")*60;
				dayCount = (int) inGameTime/dayLength;
				int currentDayTime = inGameTime-dayCount*dayLength;
				long currentDayTicks = (long) Math.floor(24000L*((double) currentDayTime/dayLength));
				
				plugin.getServer().getWorld("world").setTime(currentDayTicks);
				if (dayCount > old_dayCount) { //normalement, cela ce déclenche à la première update après le passage au matin donc pas besoin de check pour l'heure de la journée
					
					LinkedList<LG_Player> players = plugin.player_manager.getLGPlayers();		
					
					//Montreur d'ours
					LinkedList<LG_Player> tmp_players = plugin.player_manager.getLGPlayers(LG_Roles.MONTREUR_DOURS); 	
					for (int i=0;i<tmp_players.size();i++) {
						for (int u=0;u<players.size();u++) {
							if (tmp_players.get(i).getPlayer().getLocation().distance(players.get(u).getPlayer().getLocation()) < 30 && players.get(u).getRole().getCamp() == 1)
								LG_Utils.broadCastMSG(ChatColor.GOLD + " Montreur d'ours " + Integer.toString(i) + " : Grrrrrrrrrrrr!");
						}
						
					}
					
					//Petite Fille et Loup Perfide
					tmp_players = plugin.player_manager.getLGPlayers(LG_Roles.PETITE_FILLE);
					tmp_players.addAll(plugin.player_manager.getLGPlayers(LG_Roles.LOUP_PERFIDE));
					for (int i=0;i<tmp_players.size(); i++)
						tmp_players.get(i).getPlayer().removePotionEffect(PotionEffectType.INVISIBILITY);
					
					//Ouverture des votes
					if(dayCount >= plugin.getConfig().getInt("General.ouverture_vote_ep")) {
						vote_ouvert = true;
					}
					
					//On enlève force aux loups
					tmp_players = plugin.player_manager.getLGPlayers(LG_Roles.LOUP_GAROU);
					tmp_players.addAll(plugin.player_manager.getLGPlayers(LG_Roles.LOUP_GAROU_BLANC));
					tmp_players.addAll(plugin.player_manager.getLGPlayers(LG_Roles.VILAIN_PETIT_LOUP));
					tmp_players.addAll(plugin.player_manager.getLGPlayers(LG_Roles.INFECT_PDLOUPS));
					for (int i=0;i<tmp_players.size(); i++)
						tmp_players.get(i).getPlayer().removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
	
					
					
				}
				else if (currentDayTime >= dayLength/2) { //Si la nuit vient de tomber
					
					vote_ouvert = false;
					
					//On Met la force aux loups
					LinkedList<LG_Player> tmp_players = plugin.player_manager.getLGPlayers(LG_Roles.LOUP_GAROU);
					tmp_players.addAll(plugin.player_manager.getLGPlayers(LG_Roles.LOUP_GAROU_BLANC));
					tmp_players.addAll(plugin.player_manager.getLGPlayers(LG_Roles.VILAIN_PETIT_LOUP));
					tmp_players.addAll(plugin.player_manager.getLGPlayers(LG_Roles.INFECT_PDLOUPS));
					for (int i=0;i<tmp_players.size(); i++)
						tmp_players.get(i).getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 999999999,0, false, false, false));
				}
			}
		}
}
