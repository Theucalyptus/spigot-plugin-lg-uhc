package com.Theucalyptus.plugin_lg.game;

import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.GameRule;
import org.bukkit.Location;

import com.Theucalyptus.plugin_lg.LG;
import com.Theucalyptus.plugin_lg.LG_Utils;
import com.Theucalyptus.plugin_lg.data.LG_GameState;
import com.Theucalyptus.plugin_lg.players.LG_Player;

import java.util.concurrent.ThreadLocalRandom;

public class LG_GameManager {
	
	private final LG plugin;
	private LG_Game lg_game;
	private LG_GameState gameState;
	
	public LG_GameManager(LG plugin) {
		this.plugin = plugin;
	}
	
	public void resetGame() {
		plugin.player_manager.lockPlayersList(false);
		gameState = LG_GameState.STOPPED;
		lg_game.stopMainRunnable();
		
	}
	
	public int startGame() {
		
		if (gameState == LG_GameState.IN_GAME) {
				return 0;
		}
		else if( plugin.player_manager.getPlayersNbr() != 0) {

			
			//Attribution des roles:
			boolean result = plugin.player_manager.dispatchRoles(plugin.player_manager.genListeRoles());
			if (result == false) {
				return 2;
			}
			
			//WorldBorder:
			World world = plugin.getServer().getWorld("world");
			WorldBorder worldBorder = world.getWorldBorder();
			List<String> temp = (List<String>) plugin.getConfig().getStringList("Bordure.coord_centre");
			float[] coordCenter =  {Float.parseFloat(temp.get(0)), Float.parseFloat(temp.get(1))};
			Location centerLocation = new Location(plugin.getServer().getWorld("world"), coordCenter[0], 120f, coordCenter[1]);
			worldBorder.setCenter(centerLocation);
			worldBorder.setSize( plugin.getConfig().getDouble("Bordure.taille_initiale"));
			
			//Temps et Météo:
			world.setTime(0);
			world.setWeatherDuration(999999999);
			world.setStorm(false);

			//Gamerules:
			world.setGameRule(GameRule.KEEP_INVENTORY, true);
			world.setGameRule(GameRule.DO_IMMEDIATE_RESPAWN, true);
			world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
			world.setGameRule(GameRule.NATURAL_REGENERATION, false);
			world.setGameRule(GameRule.DO_INSOMNIA, true);
			world.setGameRule(GameRule.SHOW_DEATH_MESSAGES, false);
			world.setGameRule(GameRule.DISABLE_RAIDS, true);
			world.setGameRule(GameRule.FALL_DAMAGE, false);
			
			
			//Spawns et TP
			ArrayList<Location> spawns = LG_Utils.genSpawnList( plugin.getConfig().getInt("Border.taille_initiale"), centerLocation, plugin.player_manager.getPlayersNbr());
			
			
			//Actions spécifiques à chaque joueur:
			
			LinkedList<LG_Player> players = plugin.player_manager.getLGPlayers();
			for (int i=0;i<players.size();i++) {
				Player player = players.get(i).getPlayer(); //récup de l'objet joueur
								
				player.sendTitle(ChatColor.GREEN + "Lancement de la partie", ChatColor.BLUE + "La partie est actuellement en cours de lancement.", 1, 120, 30); //message de lancement
				int spawnIndex = ThreadLocalRandom.current().nextInt(0, spawns.size()); //téléportation au spawn
				player.teleport(spawns.remove(spawnIndex));
				
				player.setGameMode(GameMode.SURVIVAL); //set du gamemode
				player.getInventory().clear();
				player.setFoodLevel(20);
			}
			
			//Si tout c'est bien passé, alors on passe en mode partie en cours
			plugin.player_manager.lockPlayersList(true);
			gameState = LG_GameState.IN_GAME;
			lg_game = new LG_Game(plugin);
			lg_game.startMainRunnable();
			return 1;

		}
		else {
			return 2;
		}

				
	}
	
	public void pauseGame() { 
		if (gameState == LG_GameState.IN_GAME) {
			gameState = LG_GameState.PAUSED;
			LG_Utils.broadCastTitle(ChatColor.DARK_RED + "PAUSE", ChatColor.GOLD + "La partie est actuellement en pause.", 1, 200, 5);
			
			LinkedList<LG_Player> players = plugin.player_manager.getLGPlayers();
			for (int i=0;i<players.size();i++) {
				players.get(i).getPlayer().setAllowFlight(true);
			}
			
			Bukkit.getWorld("world").setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
		}
	
	}
	
	public void resumeGame() {
		if (gameState == LG_GameState.PAUSED) {
			gameState = LG_GameState.IN_GAME;
			LG_Utils.broadCastTitle(ChatColor.DARK_RED + "ATTENTION", ChatColor.GOLD + "La partie vient de recommencer !!.", 1, 200, 5);

			LinkedList<LG_Player> players = plugin.player_manager.getLGPlayers();
			for (int i=0;i<players.size();i++) {
				players.get(i).getPlayer().setAllowFlight(false);
			}
			
			Bukkit.getWorld("world").setGameRule(GameRule.DO_DAYLIGHT_CYCLE, true);
		}
	}
	
	public boolean isPaused() {
		boolean result = false;
		if (gameState == LG_GameState.PAUSED) 
			result = true;
		return result; 
	}

	public boolean isInGame() {
		if (gameState == LG_GameState.IN_GAME || gameState == LG_GameState.PAUSED) 
			return true;
		else
			return false;
	}
	
	public LG_Game getGame() {
		return lg_game;
	}
	
}
