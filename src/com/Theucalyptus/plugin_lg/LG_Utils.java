package com.Theucalyptus.plugin_lg;

import java.util.ArrayList;
import java.util.LinkedList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.Theucalyptus.plugin_lg.data.LG_ChatMarkers;
import com.Theucalyptus.plugin_lg.players.LG_Player;

public class LG_Utils {

	
	
	public static ArrayList<Location> genSpawnList(int borderSize, Location borderCenter, int numberPlayer) {
		
		ArrayList<Location> spawnList = new ArrayList<Location>();
		int spawnPerRaw = (int) Math.ceil(Math.sqrt((double) numberPlayer));
		int distance = (int) (borderSize-500)/spawnPerRaw;
		int bottom = (int) Math.ceil(0-spawnPerRaw/2);
		int top = bottom + spawnPerRaw;
		for (int x=bottom;x<top;x++) {
			for (int y=bottom;y<top;y++) {
				spawnList.add(borderCenter.clone().add(new Vector(x*distance, 50, y*distance)));
			}
		}
		
		return spawnList;
	}
	
	
	public static void broadCastMSG(String message) {
		Bukkit.broadcastMessage(LG_ChatMarkers.LG_Base.toString() + message);
	}

	public static void sendMSG(String message, Player player) {
		player.sendMessage(LG_ChatMarkers.LG_Base.toString() + message);
	}

	public static void broadCastTitle(String titre, String message, int fadeIn, int stay, int fadeOut) {
		LinkedList<LG_Player> players = LG.getPlugin(LG.class).player_manager.getLGPlayers();
		for (int i=0;i<players.size();i++) {
			players.get(i).getPlayer().sendTitle(titre, message, fadeIn, stay, fadeOut);
		}
	}
}
