package com.Theucalyptus.plugin_lg.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

import com.Theucalyptus.plugin_lg.LG;
import com.Theucalyptus.plugin_lg.LG_Utils;
import com.Theucalyptus.plugin_lg.data.LG_ChatMarkers;

public class CommandLG implements CommandExecutor {
	
	
	private final LG plugin;
	public CommandLG(LG plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		//Si pas de sous commandes passé en argument, on affiche l'aide
		if( args.length == 0) {
			args = new String[] {"help"};
		}
		
		switch(args[0]) {
			// /lg join -> pour s'inscrire
			case "join":  
				if(sender instanceof Player) {
					Player player = (Player) sender;
					if (plugin.player_manager.addLGPlayer(player.getUniqueId()) == true) {
						LG_Utils.sendMSG("Vous êtes désormais inscrit à la partie", player);
					}else {
						LG_Utils.sendMSG("Vous êtes déjà inscrit. ", player);
					}
					
				}
				else { 
					sender.sendMessage(LG_ChatMarkers.LG_Base.toString() + "Cette commande n'est utilisable que par les joueurs.");
					return false; 
				}
				break;
			
			case "quit":
				if(sender instanceof Player) {
					plugin.player_manager.removeLGPlayer(    ((Player) sender).getUniqueId()      );
					LG_Utils.sendMSG("Vous n'êtes plus inscrit à la partie.", (Player) sender);
					}
					else { 
					sender.sendMessage(LG_ChatMarkers.LG_Base.toString() + "Cette commande n'est utilisable que par les joueurs.");
					return false; 
				}
				
				break;
			
			case "start":
				
				String raison = "";
				
				int result = plugin.game_manager.startGame();
				if (result != 1) {
					if (result == 0) {
						raison = "une partie déjà en cours.";
					}
					else if (result == 2) {
						raison = "Le nombre de joueurs n'est pas égal au nombre de roles configurés: " + ChatColor.AQUA + plugin.player_manager.getPlayersNbr() + " joueurs inscrits, " + ChatColor.BLUE + plugin.player_manager.genListeRoles().size() + " roles configurés."; 
					}
					sender.sendMessage(LG_ChatMarkers.LG_Base.toString() + ChatColor.RED + "Impossible de lancer la partie. Raison : " + raison);
				}
				
				
				break;
			
			case "stop":
				plugin.game_manager.resetGame();
				break;
			case "pause":
				plugin.game_manager.pauseGame();
				break;
			case "resume":
				plugin.game_manager.resumeGame();
				break;

				
			case "help": // /lg help
				
				if( args.length >= 2) {
					if (args[1] == "roles") {
						sender.sendMessage(LG_ChatMarkers.LG_Base.toString() +"Utilisation : /lg <sous-commande> <pseudo>" + ChatColor.GRAY + "(situationnel)");
						sender.sendMessage(ChatColor.AQUA + "  -" +ChatColor.RED + "voir (+pseudo)" +ChatColor.GREEN +": permet de s'inscrire à la partie");
						sender.sendMessage(ChatColor.AQUA + "  -" +ChatColor.RED + "vote (+pseudo)" +ChatColor.GREEN +": permet de se de-inscrire de la partie");
						sender.sendMessage(ChatColor.AQUA + "  -" +ChatColor.RED +  "revive (+pseudo)" +ChatColor.GREEN +": lance la partie");
						sender.sendMessage(ChatColor.AQUA + "  -" +ChatColor.RED + "modele (+pseudo)" +ChatColor.GREEN +": met la partie en pause");
					}
				}
				else {
					
					sender.sendMessage(LG_ChatMarkers.LG_Base.toString() +"Utilisation : /lg <sous-commande> <pseudo>" + ChatColor.GRAY + "(situationnel)");
					sender.sendMessage(ChatColor.AQUA + "  -" +ChatColor.RED + "join" +ChatColor.GREEN +": permet de s'inscrire à la partie");
					sender.sendMessage(ChatColor.AQUA + "  -" +ChatColor.RED + "quit" +ChatColor.GREEN +": permet de se de-inscrire de la partie");
					sender.sendMessage(ChatColor.AQUA + "  -" +ChatColor.RED +  "start" +ChatColor.GREEN +": lance la partie");
					sender.sendMessage(ChatColor.AQUA + "  -" +ChatColor.RED + "pause" +ChatColor.GREEN +": met la partie en pause");
					sender.sendMessage(ChatColor.AQUA + "  -" +ChatColor.RED +  "resume"+ChatColor.GREEN +" : relance la partie mise en pause");
					sender.sendMessage(ChatColor.AQUA + "  -" +ChatColor.RED + "help" +ChatColor.GREEN +": affiche cette aide");
					sender.sendMessage(ChatColor.AQUA + "  -" +ChatColor.RED + "infos"+ChatColor.GREEN +" : affiche des informations sur le plugin");
					sender.sendMessage("Pour voir la liste des commandes relatives aux rôles, tapez /lg help roles");
				}
				
			
				break;	
			case "reload":
				sender.sendMessage("Rechargement de la configuration en cours...");
				plugin.reloadConfig();
				sender.sendMessage("Rechargement de la configuration terminé !");
	
			default: // Erreur : commande inconnue.
				sender.sendMessage(LG_ChatMarkers.LG_Base.toString() + "Commande Invalide ! Tapez /lg help pour consulter l'aide");
				break;
		}	
		return true;
	}

}
