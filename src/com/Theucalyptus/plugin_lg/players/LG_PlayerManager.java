package com.Theucalyptus.plugin_lg.players;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.ChatColor;

import com.Theucalyptus.plugin_lg.LG;
import com.Theucalyptus.plugin_lg.LG_Utils;
import com.Theucalyptus.plugin_lg.data.LG_Roles;

public class LG_PlayerManager {

	
	private final LG plugin;
	private LinkedList<LG_Player> players = new LinkedList<LG_Player>();
	private boolean locked;
	
	//Constructeur
	public LG_PlayerManager(LG plugin) {
		this.plugin = plugin;
	}

	//Ajoute un joueur à la liste des joueurs inscrits
	public boolean addLGPlayer(UUID player_uuid) {
		if (!locked && this.getLGPlayer(player_uuid) == null) {
			players.add(new LG_Player(plugin.getServer().getPlayer(player_uuid)));
			return true;
		}
		return false;	
	}
	
	public void removeLGPlayer(UUID player_uuid) {
		players.remove(this.getLGPlayer(player_uuid));
	}
	
	public ArrayList<LG_Roles> genListeRoles() {
		ArrayList<LG_Roles> liste_roles = new ArrayList<LG_Roles>();
		
		for(int i=0;i< plugin.getConfig().getInt("Composition.simple_villageois"); i++) {liste_roles.add(LG_Roles.VILLAGEOIS);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.loup_garou"); i++) {liste_roles.add(LG_Roles.LOUP_GAROU);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.loup_garou_blanc"); i++) {liste_roles.add(LG_Roles.LOUP_GAROU_BLANC);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.assassin"); i++) {liste_roles.add(LG_Roles.ASSASSIN);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.citoyen"); i++) {liste_roles.add(LG_Roles.CITOYEN);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.cupidon"); i++) {liste_roles.add(LG_Roles.CUPIDON);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.soeurs"); i++) {liste_roles.add(LG_Roles.SOEURS);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.montreur_dours"); i++) {liste_roles.add(LG_Roles.MONTREUR_DOURS);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.petite_fille"); i++) {liste_roles.add(LG_Roles.PETITE_FILLE);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.sorciere"); i++) {liste_roles.add(LG_Roles.SORCIERE);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.voyante"); i++) {liste_roles.add(LG_Roles.VOYANTE);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.infect_pdloups"); i++) {liste_roles.add(LG_Roles.INFECT_PDLOUPS);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.vilain_petit_loup"); i++) {liste_roles.add(LG_Roles.VILAIN_PETIT_LOUP);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.enfant_sauvage"); i++) {liste_roles.add(LG_Roles.ENFANT_SAUVAGE);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.renard"); i++) {liste_roles.add(LG_Roles.RENARD);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.loup_perfide"); i++) {liste_roles.add(LG_Roles.LOUP_PERFIDE);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.voleur"); i++) {liste_roles.add(LG_Roles.VOLEUR);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.ange"); i++) {liste_roles.add(LG_Roles.ANGE);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.chaman"); i++) {liste_roles.add(LG_Roles.CHAMAN);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.corbeau"); i++) {liste_roles.add(LG_Roles.CORBEAU);}
		for(int i=0;i< plugin.getConfig().getInt("Composition.loup_amnesique"); i++) {liste_roles.add(LG_Roles.LOUP_AMNESIQUE);}
		
		return liste_roles;
	}
	
	
	//Attribut les roles aux joueurs
	public boolean dispatchRoles(ArrayList<LG_Roles> liste_roles) {
				
		if(this.getPlayersNbr() != liste_roles.size()) {
			return false;
		}
		else {
			for (int i=0; i<this.getPlayersNbr(); i++) {
				int randomRoleIndex = ThreadLocalRandom.current().nextInt(0, liste_roles.size());
				LG_Roles role = liste_roles.remove(randomRoleIndex);
				players.get(i).setRole(role);
				LG_Utils.sendMSG("Vous êtes " + ChatColor.YELLOW + role.getNom() + ". " + ChatColor.RESET + role.getDescription(), players.get(i).getPlayer());
			}
			
			return true;

		}
		
	}
	
	//Retourne le nombre total de joueur inscrits
	public int getPlayersNbr() {
		return players.size();
	}
	//Retourne le LG_Player d'un joueurs via son UUID
	public LG_Player getLGPlayer(UUID player_uuid) {
		for (int i=0; i< players.size(); i++) {
			if(players.get(i).getPlayer().getUniqueId() == player_uuid) {
				return players.get(i);
			}
		}
		return null;
	}
	//Retourne la liste de tout les joueurs de la partie
	public  LinkedList<LG_Player> getLGPlayers() {
		return  (LinkedList<LG_Player>) players;
	}
	//Recherche de jouers en fonctions de leurs roles
	public LinkedList<LG_Player> getLGPlayers(LG_Roles role) {
		LinkedList<LG_Player> temp = new LinkedList<LG_Player>();
		for (int i=0;i<players.size();i++) {
			if (players.get(i).getRole() == role) {
				temp.add(players.get(i));
			}
		}
		return temp;
	}
	
	
	
	
	//Vérouille(ou dé-...) la liste des joueurs
	public boolean lockPlayersList(boolean var) {
		locked = var;
		return locked;
	}
}
