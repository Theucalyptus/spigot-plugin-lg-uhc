package com.Theucalyptus.plugin_lg.players;

import org.bukkit.entity.Player;
import com.Theucalyptus.plugin_lg.data.LG_Roles;

public class LG_Player {
	
	private LG_Roles role;
	private Player player;
	private LG_Player couple; 
	
	private boolean pouvoirUtilise = false;
	
	//Constructor
	public LG_Player(Player player) {
		this.player = player;
	}

	public LG_Roles getRole() {
		return role;
	}
	public void setRole(LG_Roles role) {
		this.role = role;
	}
	public Player getPlayer() {
		return player;
	}
	public void setCouple(LG_Player player) {
		couple = player;
	}
	public LG_Player getCouple() {
		if (couple == null) {
			return null;
		}
		return couple;
	}
	
	public void setPouvoirUtilise(boolean value) {
		pouvoirUtilise = value;
	}
	public boolean isPouvoirUsed() {
		return pouvoirUtilise;
	}
}
