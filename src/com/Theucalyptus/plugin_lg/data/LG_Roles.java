package com.Theucalyptus.plugin_lg.data;

public enum LG_Roles {
	
	//ROLE("nom", "ID", "camp", "description")
	/* Camp:  0 = village, 1 = loup, 2=solo(ou autres #couple+cupi et #role qui change de camps genre Enfant Sauvage) */
	
	//TODO: Finir les descriptions/vérifier pour l'existance de fautes d'orthographes
	
	VILLAGEOIS(			"Simple vilageois", 		1,  0, 	"Vous faîtes partie du camps du village. Votre but est d'éliminer les loups."),
	LOUP_GAROU(			"Loup-garou", 				2,  1,	"Vous faîtes partie du camps des loups. Votre objectif est d'éléminer les membres du village."),
	LOUP_GAROU_BLANC(	"Loup-Garou Blanc", 		3,  2, 	"Vous ne faîte partie d'aucun camp. Vous devez gagnez seul. Pour cela, exterminez tout le monde !"),
	ASSASSIN(			"Assassin",					4,  2, 	"Vous ne faîte partie d'aucun camp. Vous devez gagnez seul. Pour cela, exterminez tout le monde !"),
	CITOYEN(			"Citoyen", 					5,  0, 	"Vous faîtes partie du camps du village. Votre but est d'éliminer les loups."),
	CUPIDON(			"Cupidon", 					6,  2, 	"Vous ne faîte partie d'aucun camp. Vous devez gagnez seul. Pour cela, exterminez tout le monde !"),
	SOEURS(				"Sœur",						7,  0, 	"Vous faîtes partie du camps du village. Votre but est d'éliminer les loups."),
	MONTREUR_DOURS(		"Montreur d'ours",			8,  0, 	"Vous faîtes partie du camps du village. Votre but est d'éliminer les loups."),
	PETITE_FILLE(		"Petite Fille",				9,  0, 	"Vous faîtes partie du camps du village. Votre but est d'éliminer les loups."),
	SORCIERE(			"Sorcière", 				10, 0, 	"Vous faîtes partie du camps du village. Votre but est d'éliminer les loups."),
	VOYANTE(			"Voyante", 					11, 0, 	"Vous faîtes partie du camps du village. Votre but est d'éliminer les loups."),
	INFECT_PDLOUPS( 	"Infect père des loups",	12, 1,  "Vous faîtes partie du camps des loups. Votre objectif est d'éléminer les membres du village."),
	VILAIN_PETIT_LOUP( 	"Vilain petit loup",		13, 1,  "Vous faîtes partie du camps des loups. Votre objectif est d'éléminer les membres du village."),
	ENFANT_SAUVAGE( 	"Enfant Sauvage",			14, 0,  "Vous faîtes partie du camps des loups. Votre objectif est d'éléminer les membres du village."),
	RENARD( 			"Renard",					15, 0,  "Vous faîtes partie du camps des loups. Votre objectif est d'éléminer les membres du village."),
	LOUP_PERFIDE( 		"Loup perfide",				16, 1,  "Vous faîtes partie du camps des loups. Votre objectif est d'éléminer les membres du village."),
	VOLEUR( 			"Voleur",					17, 2,  "Vous faîtes partie du camps des loups. Votre objectif est d'éléminer les membres du village."),
	CHAMAN( 			"Chaman",					18, 0,  "Vous faîtes partie du camps du village. Votre but est d'éliminer les loups."),
	ANGE( 				"Ange",						19, 1,  "Vous faîtes partie du camps des loups. Votre objectif est d'éléminer les membres du village."),
	CORBEAU( 			"Corbeau",					20, 0,  "Vous faîtes partie du camps du village. Votre but est d'éliminer les loups."),
	LOUP_AMNESIQUE(		"Loup amnésique",			21, 0,  "Vous faîtes partie du camps des loups. Votre objectif est d'éléminer les membres du village.");


	
	private String nom = "";
	private int id = -1;
	private String description = "";
	private int camp = -1;
	
	//Constructeur
	LG_Roles(String nom, int id, int camp, String description) {
		this.nom = nom;
		this.id = id;
		this.description = description;
		this.camp = camp;
	}

	public String getNom() {
		return nom;
	}
	public String getDescription() {
		return description;
	}
	public int getID() {
		return id;
	}
	public int getCamp() {
		return camp;
	}
}
