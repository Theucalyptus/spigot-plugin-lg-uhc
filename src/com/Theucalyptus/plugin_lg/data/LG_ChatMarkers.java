package com.Theucalyptus.plugin_lg.data;

import org.bukkit.ChatColor;

public enum LG_ChatMarkers {
	
	LG_Base(ChatColor.WHITE + "[" + ChatColor.RED + "LG" + ChatColor.WHITE + "] " + ChatColor.RESET);
	
	private String content = "";
	LG_ChatMarkers(String contenu) {
		this.content = contenu;
	}
	
	
	public String toString() {
		return content;
	}
	
	
	

}
