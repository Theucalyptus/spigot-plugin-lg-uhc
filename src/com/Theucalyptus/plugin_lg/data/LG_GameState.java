package com.Theucalyptus.plugin_lg.data;

public enum LG_GameState {
	IN_GAME,
	PAUSED,
	STOPPED;
}
