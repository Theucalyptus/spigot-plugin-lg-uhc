package com.Theucalyptus.plugin_lg;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.Theucalyptus.plugin_lg.commands.CommandLG;
import com.Theucalyptus.plugin_lg.game.LG_GameManager;
import com.Theucalyptus.plugin_lg.players.LG_PlayerManager;

public class LG extends JavaPlugin {
	
	public  LG_PlayerManager player_manager; 
	public  LG_GameManager game_manager; 

	FileConfiguration config = this.getConfig();

	
	@Override
	public void onEnable() {
		player_manager = new LG_PlayerManager(this);	
		game_manager = new LG_GameManager(this);	

		this.getCommand("lg").setExecutor(new CommandLG(this));
	    this.getServer().getPluginManager().registerEvents(new LG_EventHandlers(this), this);
	
		//Config par défaut
	    config.addDefault("General.duree_episode", 5); // en minute
	    config.addDefault("General.ouverture_vote_ep", 3); //en jours inGame
	    config.addDefault("General.voyante_bavarde", false);
	    config.addDefault("General.pseudo_visible", true);
	    
	    config.addDefault("Composition.simple_villageois", 2);
		config.addDefault("Composition.loup_garou", 2);
		config.addDefault("Composition.loup_garou_blanc", 1);
		config.addDefault("Composition.assassin", 1);
		config.addDefault("Composition.citoyen", 1);
		config.addDefault("Composition.cupidon", 1);
		config.addDefault("Composition.soeurs", 1);
		config.addDefault("Composition.montreur_dours", 1);
		config.addDefault("Composition.petite_fille", 1);
		config.addDefault("Composition.sorciere", 1);
		config.addDefault("Composition.voyante", 1);
		config.addDefault("Composition.infect_pdloups", 1);
		config.addDefault("Composition.vilain_petit_loup", 1);
		config.addDefault("Composition.enfant_sauvage", 1);
		config.addDefault("Composition.loup_perfide", 1);
		config.addDefault("Composition.renard", 1);
		config.addDefault("Composition.chaman", 1);
		config.addDefault("Composition.ange", 1);
		config.addDefault("Composition.loup_amnesique", 1);
		config.addDefault("Composition.corbeau", 1);
		config.addDefault("Composition.voleur", 1);




	
		
		
		float[] coordCenter = {0.0f,0.0f}; // x0 y0 (en bloc)
		config.addDefault("Bordure.coord_centre", coordCenter);
		config.addDefault("Bordure.taille_initiale", 8000.0); // +2000 +2000 / -2000 -2000 rel. au centre (en bloc)
		config.addDefault("Bordure.taille_finale", 300.0);
		config.addDefault("Bordure.temps_debut_reduction", 100); //en minute
		config.addDefault("Bordure.duree_reduction", 30); //en minute
		
		
		config.options().copyDefaults(true);
		saveConfig();
		
	}
	
	
	@Override
	public void onDisable() {
		
	}
	
	public void Main() {
		new BukkitRunnable() {
		    public void run() {
		        
		    }
		}.runTaskTimer(this, 0L, 5L);
		
	}
	

}
